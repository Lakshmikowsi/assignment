package com.gjj.igden.service.userservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gjj.igden.dao.UserDao;
import com.gjj.igden.model.Account;

@Service
public class UserService implements UserDetailsService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException {
    	logger.debug("In UserDetails,load user by username::" + accountName);
        Account account = userDao.findAccountByName(accountName);

        if (account == null) {
            throw new UsernameNotFoundException(String.format("User with username=\"%s\" does not exist", accountName));
        }

        if (!account.isEnabled()) {
            throw new IllegalArgumentException(String.format("User not enabled"));
        }
        return account;
    }
}
